## 全局命令

查看所有键

```
keys *
```

键总数

```
dbsize
```

键是否存在
```
exists key
```

删除键
```
del key1 key2
```

键的数据类型
```
type key
```

键重命名
```
rename key newkey
```

随机返回一个键
```
randomkey
```

键过期
```
# 键在seconds秒后过期
expire key seconds

# 键在秒级时间戳timestamp后过期
expireat key timestamp

ttl key
```

迁移键, 迁移键功能非常重要，因为有时候我们只想把部分数据由一个Redis迁移到另一个Redis（例如从生产环境迁移到测试环境），Redis发展历程中提供了move、dump+restore、migrate三组迁移键的方法，它们的实现方式以及使用的场景不太相同。
```
# 用于在redis内部迁移数据
move key db
```
```
# 两步
# 1.在源redis,dump命令会将键值序列化, 格式采用RDB格式
dump key
# 2.在目标redis, restore命令将上面序列化的值进行复原, 其中ttl参数代表过期时间, 如果ttl=0代表没有过期时间
restore key ttl value
```

## string

字符串, 其他几种数据类型都是在字符串的基础上构建的, 字符串的值可以是字符串, 数字, 甚至是二进制图片音视频等, 但是最大值不能超过512M。

#### 常用命令
```
set key value [ex seconds] [px milliseconds] [nx|xx]

ex seconds : 为键设置秒级过期时间
px milliseconds : 为键设置毫秒级过期时间
nx : 键必须不存在, 才可以设置成功, 用于添加
xx : 键必须存在, 才可以设置成功, 用于更新
```
```
set name aaa ex 10
```

```
# 批量
mset a 1 b 2 c 3
mget a b c 
```
```
# 删除
del key
del key1 key2
```
```
# 设置过期时间, 秒
expire name 10
```
```
# 追加
append key value
```
```
# 长度
strlen key
```
```
# 获取部分字符串
getrange key start end
```
```
# 存入一个不存在的字符串键值对, 不存在返回1, 存在返回0
setnx key value
```
#### 原子操作
```
incr num
decr num
incrby num 2
decrby num 2
```

#### 内部编码

字符串类型有3种内部编码, redis会根据当前值的类型和长度决定使用哪种内部编码实现。  

int, 8个字节的长整型;   
embstr, 小于等于39个字节的字符串;   
raw, 大于39个字节的字符串;  

#### 应用场景

1. 缓存功能
```
# 单值缓存
set key value
get key
```
```
# 对象缓存
set user:1 value_json
mset user:1:name zhangsan user:1:age 11
mget user:1:name user:1:age
```
2. 分布式锁
```
# 加锁, 返回1表示加锁成功, 返回0表示失败, 加个过期时间防止程序意外终止导致死锁
setnx product:1 true ex 10

# 释放锁
del product:1
```
3. 计数
```
incr article:readcount:id
```
4. 分布式全局序列号
```
# 一次生成1000个序列号, 提升性能
incrby orderid 1000
```
5. 共享session
6. 限速, 比如限制某个ip在指定时间内只能访问多少次

## list

#### 常用命令

添加
```
rpush key v1 v2
lpush key v1 v2
# 找到等于pivot的元素, 在其前或者其后插入一个新的元素value
linsert key before|after pivot value
```
查找
```
# 获取指定范围内的元素列表
lrang key start end
```
```
# 获取指定索引下标的元素
lindex key index
```
删除
```
# 列表左侧弹出
lpop key
# 列表右侧弹出
rpop key
# 找到等于value的元素进行删除
# count>0, 从左向右, 最多删除count个元素
# count<0, 从右向左, 删除最多count绝对值个元素
# count=0, 删除所有
lrem key count value
```
```
# 只保留范围内的元素
ltrim key start end
```

修改
```
# 修改指定下标的元素
lset key index newValue
```

长度
```
llen key
```

阻塞操作

blpop和brpop是lpop和rpop的阻塞版本, 它们除了弹出方向不同, 使用方法基本基本相同

#### 内部编码

1. ziplist, 压缩列表
2. linkedlist, 链表, 当列表类型无法满足ziplist的条件时，Redis会使用linkedlist作为列表的内部实现

#### 使用场景

1. 消息队列
2. 文章列表

## set

集合中不允许有重复元素，并且集合中的元素是无序的，不能通过索引下标获取元素。

#### 常用命令  

添加
```
sadd key element [element ...]
```

删除
```
srem key element [element ...]
```

元素个数
```
# scard的时间复杂度为O(1), 它不会遍历集合所有元素, 而是直接用Redis内部的变量。
scard key
```

是否存在
```
# 存在返回返回1, 不存在返回0
sismember key element
```

随机获取指定个数元素
```
# count默认为1
srandmember key [count]
```

随机弹出元素
```
spop key
```

获取所有元素
```
smembers key
```

集合间操作
```
# 交集
sinter key [key ...]
```
```
# 并集
suinon key [key ...]
```
```
# 差集
sdiff key [key ...]
```

#### 内部编码  

#### 使用场景  

1. 用户标签

## sortedset

有序集合相对于哈希、列表、集合来说会有一点点陌生，但既然叫有序集合，那么它和集合必然有着联系，它保留了集合不能有重复成员的特性，但不同的是，有序集合中的元素可以排序。但是它和列表使用索引下标作为排序依据不同的是，它给每个元素设置一个分数（score）作为排序的依据。

#### 常用命令  

#### 内部编码  

#### 使用场景  

1. 用户点赞数
 
## hash

可以理解为Map类型

#### 常用命令  

```
hset user name zhangsan
hmset user name zhangsan age 11

hget user name
hget user name age
```
```
# 删除
hdel user name
hdel user name age
```
```
127.0.0.1:6379> hsetnx user name zhangsan
(integer) 0
```
```
# 元素个数
hlen user
```
```
# 获取所有的field
127.0.0.1:6379> hkeys user
1) "age"
2) "name"
```
```
# 获取所有的value
127.0.0.1:6379> hvals user
1) "11"
2) "zhangsan"
127.0.0.1:6379> 
```
获取所有的field-value, 在使用hgetall时，如果哈希元素个数比较多，会存在阻塞Redis的可能。如果开发人员只需要获取部分field，可以使用hmget，如果一定要获取全部field-value，可以使用hscan命令，该命令会渐进式遍历哈希类型。
```
127.0.0.1:6379> hgetall user
1) "age"
2) "11"
3) "name"
4) "zhangsan"
```

```
# 自增
127.0.0.1:6379> hincrby user age 11
(integer) 22
```
```
# 计算value的长度
127.0.0.1:6379> hstrlen user name
(integer) 8
```

#### 内部编码  

1. ziplist, 压缩列表, 当哈希类型元素个数小于hash-max-ziplist-entries配置（默认512个）、同时所有值都小于hash-max-ziplist-value配置（默认64字节）时，Redis会使用ziplist作为哈希的内部实现，ziplist使用更加紧凑的结构实现多个元素的连续存储，所以在节省内存方面比hashtable更加优秀。

2. hashtable, 哈希表, 当哈希类型无法满足ziplist的条件时，Redis会使用hashtable作为哈希的内部实现，因为此时ziplist的读写效率会下降，而hashtable的读写时间复杂度为O(1)。

#### 优缺点

1. 同类数据归类整合储存，方便数据管理
2. 相比string操作消耗内存与cpu更小
3. 相比string储存更节省空间

1. 过期功能不能使用在field上，只能用在key上
2. Redis集群架构下不适合大规模使用


#### 使用场景  

1.对象缓存
```
hmset user {userid}:name zhangsan {userid}:age 11

hmset user 1:name zhangsan 1:age 11
hmget user 1:name 1:age
```
2.购物车
```
# 用户id为key, 商品id为field, 商品数量为value
hset cart:{userid} {goods_id} goods_num

# 添加商品
hset cart:1 100 2

# 增加数量
hincrby cart:1 100 1

# 商品总数
hlen cart:1

# 删除商品
hdel cart:1 100

# 获取购物车所有商品
hgetall cart:1
```