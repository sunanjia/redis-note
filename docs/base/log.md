## 慢查询

#### 最大超时时间

单位为微秒, 默认值是10000微秒

```
slowlog-log-slower-than 10000
```

#### 最大日志数

```
slowlog-max-len 128
```

#### 获取日志

```
slowlog get [n]
```
```
slowlog get
slowlog get 1
```

#### 日志长度

```
slowlog len
```

#### 日志重置

```
slowlog reset
```

#### 最佳实践

1. 长度可以设置为1000以上
2. 默认10毫秒就会判定为慢查询, 对于高流量场景, 如果命令执行时间在1毫秒以上, 那么redis最多支撑OPS不到1000, 因此对高OPS场景的redis建议为1毫秒
