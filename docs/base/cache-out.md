如果redis的内存占用过多的时候，就会触发内存淘汰机制。
1. noeviction: 当内存不足以写入新数据时, 会报错。这个一般没人用。
2. allkeys-lru: 当内存不足以写入新数据时, 移除最少使用的key, 这个最常用。
3. allkeys-random：当内存不足以容纳新写入数据时，在键空间中，随机移除某个key，这个一般没人用吧，为啥要随机，肯定是把最近最少使用的key给干掉
4. volatile-lru: 当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，移除最近最少使用的key（这个一般不太合适）
5. volatile-random：当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，随机移除某个key
6. volatile-ttl：当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，有更早过期时间的key优先移除

```
maxmemory-policy allkeys-lru
```