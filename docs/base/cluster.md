## 代理方案

健壮性相对差，性能相对差

#### twproxy

#### codis(豌豆荚)



## Redis-Cluster

Redis集群最少需要三台主服务器，三台从服务器。

1. Redis3以后，官方的集群方案 Redis-Cluster
2. Redis3 使用lua脚本实现
3. Redis5 直接实现

#### 架构

1. 所有的redis主节点彼此互联(PING-PONG机制),内部使用二进制协议优化传输速度和带宽
2. 节点的fail是通过集群中超过半数的节点检测失效时才生效
3. 客户端与redis节点直连,不需要中间proxy层.客户端不需要连接集群所有节点,连接集群中任何一个可用节点即可
4. redis-cluster把所有的物理节点映射到[0-16383]slot上,cluster 负责维护node<->slot<->value

