## 源码安装 
#### redis.sh
```
#!/bin/bash

REDIS_VERSION=5.0.9

wget http://download.redis.io/releases/redis-${REDIS_VERSION}.tar.gz
tar zxf redis-${REDIS_VERSION}.tar.gz
mkdir -p /opt/redis/${REDIS_VERSION}
cd redis-${REDIS_VERSION}

make MALLOC=libc  
make PREFIX=/opt/redis/${REDIS_VERSION} install

mkdir -p /opt/redis/data /opt/redis/conf /opt/redis/logs
touch /opt/redis/logs/redis.log
cp redis.conf /opt/redis/conf/

echo 511 > /proc/sys/net/core/somaxconn
echo 1 > /proc/sys/vm/overcommit_memory
echo never > /sys/kernel/mm/transparent_hugepage/enabled
```
启动
```
/opt/redis/bin/redis-server /opt/redis/conf/redis.conf
```
## 配置
redis.conf
```
# 端口
port 6379

# 如果只能本机访问
# bind 127.0.0.1
# 所有ip都可访问
# bind 0.0.0.0
# 也可以配置多个ip地址
bind 127.0.0.1 192.168.64.129

# 密码
requirepass 123456

# 日志位置
logfile "/opt/redis/logs/redis.log"

# redis工作目录, 存放持久化文件
dir "/opt/redis/data/"

# 是否以守护进程的方式启动redis
daemonize yes
```
临时修改配置文件
```
config set key value
```
```
# 使生效
config rewrite
```
## 命令
```
bin/
├── redis-benchmark
├── redis-check-aof
├── redis-check-rdb
├── redis-cli
├── redis-sentinel -> redis-server
└── redis-server
```
#### redis-server
```
redis-server redis.conf
```
#### redis-cli
```
# 连接
redis-cli -h 127.0.0.1 -p 8001 -a 123456

# 关闭redis服务
# 默认关闭127.0.0.1上的6379端口
redis-cli shutdown

# 指定端口
redis-cli -h 127.0.0.1 -p 8001 shutdwon

# 是否在关闭redis前, 生成持久化文件
redis-cli shutdown nosave|save
```
#### redis-benchmark

redis-benchmark可以为Redis做基准性能测试，它提供了很多选项帮助开发和运维人员测试Redis的相关性能

|参数|描述|
|-|-|
|-c|-c（clients）选项代表客户端的并发数量（默认是50）|
|-n|-n（num）选项代表客户端请求总量（默认是100000）|
|-q|-q选项仅仅显示redis-benchmark的requests per second信息|
|-r|如果想向Redis插入更多的键，可以执行使用-r（random）选项，可以向Redis插入更多随机的键。|
|-P|-P选项代表每个请求pipeline的数据量（默认为1）|
|-k|-k选项代表客户端是否使用keepalive，1为使用，0为不使用，默认值为1。|
|-t|-t选项可以对指定命令进行基准测试|
|--csv|--csv选项会将结果按照csv格式输出，便于后续处理，如导出到Excel等。|

#### redis-check-dump

RDB持久化文件检测和修复工具

#### redis-check-aof

AOF持久化文件检测和修复文件

#### redis-sentinel

启动Redis Sentinel

## info

```
127.0.0.1:6379> info

# 服务器运行的环境参数
# Server
redis_version:5.0.9
redis_git_sha1:00000000
redis_git_dirty:0
redis_build_id:a672ac92ea5a8fe4
redis_mode:standalone
os:Linux 3.10.0-1160.15.2.el7.x86_64 x86_64
arch_bits:64
multiplexing_api:epoll
atomicvar_api:atomic-builtin
gcc_version:4.8.5
process_id:78669
run_id:6b709afd9bebabe2b81a00a68c194fb64988eac3
tcp_port:6379
uptime_in_seconds:70
uptime_in_days:0
hz:10
configured_hz:10
lru_clock:5443675
executable:/opt/redis/./5.0.9/bin/redis-server
config_file:/opt/redis/./data/conf/redis.conf

# 客户端相关信息
# Clients
connected_clients:1  # 正在连接的客户端数量
client_recent_max_input_buffer:2
client_recent_max_output_buffer:0
blocked_clients:0

# 服务器运行内存统计信息
# Memory
used_memory:855368              # 内存总量(byte), 包含redis进程内部的开销和数据占用的内存
used_memory_human:835.32K       # 内存总量(kb)
used_memory_rss:12865536
used_memory_rss_human:12.27M    # 向操作系统申请的内存大小(Mb)
used_memory_peak:855368         # redis的内存消耗峰值(byte)
used_memory_peak_human:835.32K  # redis的内存消耗峰值(KB)
used_memory_peak_perc:100.00%
used_memory_overhead:841430
used_memory_startup:791512
used_memory_dataset:13938
used_memory_dataset_perc:21.83%
allocator_allocated:1554888
allocator_active:1908736
allocator_resident:10825728
total_system_memory:1907740672
total_system_memory_human:1.78G
used_memory_lua:37888
used_memory_lua_human:37.00K
used_memory_scripts:0
used_memory_scripts_human:0B
number_of_cached_scripts:0
maxmemory:0                    # 配置中设置的最大可使用内存值(byte),默认0,不限制
maxmemory_human:0B             # 配置中设置的最大可使用内存值
maxmemory_policy:noeviction    # 当达到maxmemory时的淘汰策略
allocator_frag_ratio:1.23
allocator_frag_bytes:353848
allocator_rss_ratio:5.67
allocator_rss_bytes:8916992
rss_overhead_ratio:1.19
rss_overhead_bytes:2039808
mem_fragmentation_ratio:15.82
mem_fragmentation_bytes:12052424
mem_not_counted_for_evict:0
mem_replication_backlog:0
mem_clients_slaves:0
mem_clients_normal:49694
mem_aof_buffer:0
mem_allocator:jemalloc-5.1.0
active_defrag_running:0
lazyfree_pending_objects:0

# 持久化信息
# Persistence
loading:0
rdb_changes_since_last_save:0
rdb_bgsave_in_progress:0
rdb_last_save_time:1616056342
rdb_last_bgsave_status:ok
rdb_last_bgsave_time_sec:-1
rdb_current_bgsave_time_sec:-1
rdb_last_cow_size:0
aof_enabled:0
aof_rewrite_in_progress:0
aof_rewrite_scheduled:0
aof_last_rewrite_time_sec:-1
aof_current_rewrite_time_sec:-1
aof_last_bgrewrite_status:ok
aof_last_write_status:ok
aof_last_cow_size:0

# 通用统计数据
# Stats
total_connections_received:2
total_commands_processed:4
instantaneous_ops_per_sec:0   # 每秒执行多少次指令
total_net_input_bytes:134
total_net_output_bytes:14886
instantaneous_input_kbps:0.00
instantaneous_output_kbps:0.00
rejected_connections:0
sync_full:0
sync_partial_ok:0
sync_partial_err:0
expired_keys:0
expired_stale_perc:0.00
expired_time_cap_reached_count:0
evicted_keys:0
keyspace_hits:1
keyspace_misses:0
pubsub_channels:0
pubsub_patterns:0
latest_fork_usec:0
migrate_cached_sockets:0
slave_expires_tracked_keys:0
active_defrag_hits:0
active_defrag_misses:0
active_defrag_key_hits:0
active_defrag_key_misses:0

# 主从复制相关信息
# Replication
role:master
connected_slaves:0
master_replid:a6910067a58058588f765ca39f0f471249c6005b
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0

# CPU使用情况
# CPU
used_cpu_sys:0.073637
used_cpu_user:0.032216
used_cpu_sys_children:0.000000
used_cpu_user_children:0.000000

# 集群信息
# Cluster
cluster_enabled:0

# 键值对统计数量信息
# Keyspace
db0:keys=4,expires=0,avg_ttl=0
```


