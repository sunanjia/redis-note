```
@Autowired
private StringRedisTemplate stringRedisTemplate;

@Test
public void testLock(){
    String lockKey = "lock";
    String clientId = UUID.randomUUID().toString();
    try {

        // 给锁设置一个过期时间
        Boolean isLock = stringRedisTemplate.opsForValue().setIfAbsent(lockKey, clientId, 10, TimeUnit.SECONDS);
        if (!isLock){// 如果已经存在
            return;
        }

        int num = Integer.parseInt(stringRedisTemplate.opsForValue().get("num"));
        if (num > 0){
            num --;
            stringRedisTemplate.opsForValue().set("num",  num + "");
        }

    }finally {

        // 删除锁, 只删除自己设置的锁
        if (clientId.equals(stringRedisTemplate.opsForValue().get(lockKey))){
            stringRedisTemplate.delete(lockKey);
        }
    }
}
```
### Redisson

```
@Autowired
private StringRedisTemplate stringRedisTemplate;

@Autowired
private Redisson redisson;

@Test
public void testRedission(){
    String lockKey = "lock";
    RLock lock = redisson.getLock(lockKey);
    try {

        // 加锁
        lock.lock();
        int num = Integer.parseInt(stringRedisTemplate.opsForValue().get("num"));
        if (num > 0){
            num --;
            stringRedisTemplate.opsForValue().set("num",  num + "");
        }

    }finally {
        // 释放锁
        lock.unlock();
    }
}
```
Redisson分布式锁原理

<img :src="$withBase('/redisson-lock.png')" alt="分布式锁实现原理">



