```
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>2.9.0</version>
    <scope>compile</scope>
</dependency>
```

## 单线程
使用单例模式避免每次都去连接Redis服务器
```
@Component
public class JedisClientUtil {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.password}")
    private String password;

    private Jedis jedis;
    private byte[] lock = new byte[1];
    public Jedis getJedis(){
        if (jedis == null){
            synchronized (lock){
                if (jedis == null){
                    jedis = new Jedis(host, port);
                }
            }
        }
        jedis.auth(password);
        return jedis;
    }

}
```
```
@SpringBootTest
class JedisDemoApplicationTests {

    @Autowired
    private JedisClientUtil jedisClientUtil;

    @Test
    void testJedis() {
        Jedis jedis = jedisClientUtil.getJedis();
        try {
            jedis.set("name", "aaa");
            System.out.println(jedis.get("name"));
        }finally {
            jedis.close();
        }
    }

}
```
## 连接池 

```
@Component
public class JedisClientPoolUtil {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.password}")
    private String password;

    private byte[] lock = new byte[1];
    private JedisPool jedisPool;

    private JedisPool getJedisPool(){
        if (jedisPool == null){
            synchronized (lock){
                if (jedisPool == null){
                    jedisPool = new JedisPool(jedisPoolConfig(), host, port);
                }
            }
        }
        return jedisPool;
    }

    private JedisPoolConfig jedisPoolConfig(){
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(8);
        jedisPoolConfig.setMaxTotal(10);
        jedisPoolConfig.setMaxWaitMillis(1000);
        return jedisPoolConfig;
    }

    public Jedis getJedis(){
        Jedis jedis = getJedisPool().getResource();
        jedis.auth(password);
        return jedis;
    }

}
```
```
@SpringBootTest
public class JedisClientPoolUtilTest {

    @Autowired
    private JedisClientPoolUtil jedisClientPoolUtil;

    @Test
    public void testJedisPool(){
        Jedis jedis = jedisClientPoolUtil.getJedis();
        System.out.println(jedis.get("name"));
    }

}
```
## 高可用连接(master/slave)

```
@Component
public class JedisClientSentinePoolUtil {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.password}")
    private String password;

    private byte[] lock = new byte[1];
    private JedisSentinelPool jedisSentinelPool;

    private JedisSentinelPool getJedisPool(){
        if (jedisSentinelPool == null){
            synchronized (lock){
                if (jedisSentinelPool == null){
                    Set<String> hosts = new HashSet<String>();
                    hosts.add("192.168.11.101");
                    jedisSentinelPool = new JedisSentinelPool("", hosts);
                }
            }
        }
        return jedisSentinelPool;
    }

    public Jedis getJedis(){
        Jedis jedis = getJedisPool().getResource();
        jedis.auth(password);
        return jedis;
    }

}
```
