```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

## 使用

```
@Data
@AllArgsConstructor
public class User implements Serializable {
    private Long id;
    private String name;
    private Integer age;
}
```
```
@SpringBootTest
class SpringDataRedisDemoApplicationTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    void testObj() {
		redisTemplate.opsForValue().set("name", "张三");

		// 自动序列化和反序列化
        redisTemplate.opsForValue().set("user", new User(1L, "张三", 11));
        User user = (User)redisTemplate.opsForValue().get("user");
        System.out.println(user.toString());
    }

}
```


## 源码分析

#### org.springframework.boot.autoconfigure的spring.factories

```
org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisReactiveAutoConfiguration,\
org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration,\
```

#### RedisAutoConfiguration
Redis自动配置类
```
@Configuration
@ConditionalOnClass(RedisOperations.class) //有RedisOperations.class类的条件下才会触发自动装配
// @EnableConfigurationProperties注解的作用就是使带有@ConfigurationProperties注解的类生效。
@EnableConfigurationProperties(RedisProperties.class)
// @Import的作用主要是导入配置类
@Import({ LettuceConnectionConfiguration.class, JedisConnectionConfiguration.class })
public class RedisAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean(name = "redisTemplate")
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
		RedisTemplate<Object, Object> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}

	@Bean
	@ConditionalOnMissingBean
	public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory)
			throws UnknownHostException {
		StringRedisTemplate template = new StringRedisTemplate();
		template.setConnectionFactory(redisConnectionFactory);
		return template;
	}

}
```

#### RedisOperations

RedisOperations有两个子类, 分别是RedisTemplate和StringRedisTemplate。




