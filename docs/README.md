---
home: true
heroImage: /logo.png
heroText: redis学习笔记
tagline: redis study note
actionText: 开始学习 →
actionLink: /guide/
footer: MIT Licensed | Copyright © 2018-present
---