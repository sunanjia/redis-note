module.exports = {
    title: 'redis',
    description: 'redis note',
    base: '/redis-note/',
    themeConfig: {
        sidebar: 
        [
            {
                title: '入门篇',
                collapsable: false,
                children: [
                    {
                        title: '概述',
                        path: '/guide/'
                    },
                    {
                        title: '安装',
                        path: '/guide/install'
                    }
                ]
            },
            {
                title: '基础篇',
                collapsable: false,
                children: [
                    {
                        title: '数据类型',
                        path: '/base/data-type'
                    },
                    {
                        title: '持久化',
                        path: '/base/data-persistence'
                    },
                    {
                        title: '缓存淘汰策略',
                        path: '/base/cache-out'
                    },
                    {
                        title: '日志',
                        path: '/base/log'
                    },
                    {
                        title: '主从复制',
                        path: '/base/master-slave-copy'
                    },
                    {
                        title: '哨兵机制',
                        path: '/base/sentinel'
                    },
                    {
                        title: '集群策略',
                        path: '/base/cluster'
                    },
                    {
                        title: '常见问题',
                        path: '/base/problem'
                    },
                    {
                        title: '面试题',
                        path: '/base/interview'
                    }
                ]
            },
            {
                title: '使用篇',
                collapsable: false,
                children: [
                    {
                        title: 'Java',
                        children: [
                            {
                                title: 'Jedis',
                                path: '/code/java/jedis'
                            },
                            {
                                title: 'Spring Data Redis',
                                path: '/code/java/spring-data-redis'
                            },
                            {
                                title: '分布式锁',
                                path: '/code/java/lock'
                            }
                        ]
                    }
                ]
            }
        ]
    }
}